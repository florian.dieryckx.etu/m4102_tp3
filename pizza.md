## REST - Pizzas

Voici les différentes commandes en rapport avec la création, l'utilisation et la visualisation des pizzas.

| Verbe | URL | Action |
|-------|-----|--------|
| POST  | /pizzas | création d'une pizza |
| GET   | /pizzas | liste de toutes les pizza |
| GET   | /pizzas/{id}/name | Lit une pizza dont le nom est {id} |
| GET   | /pizzas/{id} | Lit une pizza {id} |
| PUT   | /pizzas/{id} | Met à jour une pizza {id} |
| DELETE| /pizzas/{id} | supprime le Foo identifié par {id} |
| GET   | /pizzas/{id}/ingredients | lit les ingredients d'une pizza {id} |
| PUT   | /pizzas/{id}/ingredients | met a jour les ingredients d'une pizza {id} |
| DELETE| /pizzas/{id}/ingredients | supprime tous les ingredients d'une pizza {id} |
| DELETE| /pizzas/{id}/ingredients/{idIngr} | supprime l'ingredient {idIngr} d'une pizza {id} |

