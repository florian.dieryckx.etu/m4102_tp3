package fr.ulille.iut.pizzaland.dao;

import java.util.List;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import fr.ulille.iut.pizzaland.beans.Pizza;

public interface PizzaDao {

	@SqlUpdate("CREATE TABLE IF NOT EXISTS Pizzas (id INTEGER PRIMARY KEY, name VARCHAR UNIQUE NOT NULL)")
	void createPizzaTable();

	@SqlUpdate("DROP TABLE IF EXISTS Pizzas")
	void dropTable();
	
	@SqlUpdate("CREATE TABLE IF NOT EXISTS PizzaIngredientsAssociation ("
			+ "idPizza INTEGER,"
			+ "idIngredient INTEGER,"
			+ "FOREIGN KEY(idPizza) REFERENCES Pizzas(id),"
			+ "FOREIGN KEY(idIngredient) REFERENCES ingredients(id)"
			+ ")")
	void createAssociationTable();

	@Transaction
	default void createTableAndIngredientAssociation() {
		createPizzaTable();
		createAssociationTable();
	}

	@SqlQuery("SELECT * FROM Pizzas")
	@RegisterBeanMapper(Pizza.class)
	List<Pizza> getAll();

	@SqlQuery("SELECT * FROM Pizzas WHERE id = :id")
	@RegisterBeanMapper(Pizza.class)
	Pizza findById(long id);
	
	@SqlQuery("SELECT * FROM Pizzas WHERE name = :name")
	@RegisterBeanMapper(Pizza.class)
	Pizza findByName(String name);
	
	@SqlUpdate("INSERT INTO Pizzas (name) VALUES (:name)")
	@GetGeneratedKeys
	long insert(String name);

	@SqlUpdate("DELETE FROM Pizzas WHERE id = :id")
	void remove(long id);

}
