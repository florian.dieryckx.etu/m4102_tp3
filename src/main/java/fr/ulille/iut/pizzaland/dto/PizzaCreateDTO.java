package fr.ulille.iut.pizzaland.dto;

public class PizzaCreateDTO {
	private String name;

	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
}
